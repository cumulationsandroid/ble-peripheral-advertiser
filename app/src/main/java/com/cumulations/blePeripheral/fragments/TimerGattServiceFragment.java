/*
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cumulations.blePeripheral.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.blePeripheral.GattServerActivity;
import com.cumulations.blePeripheral.R;

import java.util.Arrays;
import java.util.Date;


public class TimerGattServiceFragment extends ServiceFragment {

    public static final String TAG = TimerGattServiceFragment.class.getSimpleName();
    private ServiceFragmentDelegate mDelegate;
    /* Local UI */
    private TextView mLocalTimeView;

    // GATT
    private BluetoothGattService timerGattService;
    private BluetoothGattCharacteristic isscTxCharacteristic, isscRxCharacteristic;

    /**
     * Listens for system time changes and triggers a notification to
     * Bluetooth subscribers.
     */
    private BroadcastReceiver mTimeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            byte adjustReason;
            switch (intent.getAction()) {
                case Intent.ACTION_TIME_CHANGED:
                    adjustReason = TimeProfile.ADJUST_MANUAL;
                    break;
                case Intent.ACTION_TIMEZONE_CHANGED:
                    adjustReason = TimeProfile.ADJUST_TIMEZONE;
                    break;
                default:
                case Intent.ACTION_TIME_TICK:
                    adjustReason = TimeProfile.ADJUST_NONE;
                    break;
            }
            long now = System.currentTimeMillis();
            notifyRegisteredDevices(now, adjustReason);
            updateLocalUi(now);
        }
    };

    public TimerGattServiceFragment() {
        timerGattService = TimeProfile.createTimeService();
    }

    // Lifecycle callbacks
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_timer_gatt, container, false);
        mLocalTimeView = (TextView) view.findViewById(R.id.text_time);
        // Initialize the local UI
        updateLocalUi(System.currentTimeMillis());
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDelegate = (ServiceFragmentDelegate) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ServiceFragmentDelegate");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDelegate = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Register for system clock events
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        getActivity().registerReceiver(mTimeReceiver, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(mTimeReceiver);
    }

    public BluetoothGattService getBluetoothGattService() {
        return timerGattService;
    }


    @Override
    public ParcelUuid getServiceUUID() {
        return new ParcelUuid(timerGattService.getUuid());
    }

    @Override
    public void notificationsEnabled(BluetoothGattCharacteristic characteristic, boolean indicate) {
        if (indicate) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), R.string.notificationsEnabled, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    @Override
    public void notificationsDisabled(BluetoothGattCharacteristic characteristic) {

    }

    @Override
    public int writeCharacteristic(final BluetoothGattCharacteristic characteristic, int offset, final byte[] value) {
        if (offset != 0) {
            return BluetoothGatt.GATT_INVALID_OFFSET;
        }
        characteristic.setValue(value);
        return BluetoothGatt.GATT_SUCCESS;
    }

    /**
     * Send a time service notification to any devices that are subscribed
     * to the characteristic.
     */
    private void notifyRegisteredDevices(long timestamp, byte adjustReason) {
        byte[] exactTime = TimeProfile.getExactTime(timestamp, adjustReason);
        Log.d(TAG,"notifyRegisteredDevices timestamp "+timestamp+" adjustReason "+adjustReason+" exactTime "+ Arrays.toString(exactTime));

        BluetoothGattServer mBluetoothGattServer = ((GattServerActivity)getActivity()).mGattServer;
        BluetoothGattCharacteristic timeCharacteristic = mBluetoothGattServer
                .getService(TimeProfile.TIME_SERVICE)
                .getCharacteristic(TimeProfile.CURRENT_TIME);
        timeCharacteristic.setValue(exactTime);
        mDelegate.sendNotificationToDevices(timeCharacteristic);
    }

    /**
     * Update graphical UI on devices that support it with the current time.
     */
    private void updateLocalUi(long timestamp) {
        Date date = new Date(timestamp);
        String displayDate = DateFormat.getMediumDateFormat(getActivity()).format(date)
                + "\n"
                + DateFormat.getTimeFormat(getActivity()).format(date);
        mLocalTimeView.setText(displayDate);
    }
}
