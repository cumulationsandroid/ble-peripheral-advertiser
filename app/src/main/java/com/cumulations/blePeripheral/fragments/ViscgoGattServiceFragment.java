/*
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cumulations.blePeripheral.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import com.cumulations.blePeripheral.GattServerActivity;
import com.cumulations.blePeripheral.R;


public class ViscgoGattServiceFragment extends ServiceFragment {

    public static final UUID VISCGO_SERVICE_UUID = UUID.fromString("5541fa45-03dc-4073-b3d9-e0ddca2b6bf8");
    public static final UUID VISCGO_READ_UUID = UUID.fromString("5541fa45-04dc-4073-b3d9-e0ddca2b6bf8");
    public static final UUID VISCGO_WRITE_UUID = UUID.fromString("5541fa45-05dc-4073-b3d9-e0ddca2b6bf8");
    public static final UUID VISCGO_NOTIFY_UUID = UUID.fromString("5541fa45-06dc-4073-b3d9-e0ddca2b6bf8");

    private static final int INITIAL_VISCGO_LEVEL = 50;
    private static final int VISCGO_LEVEL_MAX = 100;

    private ServiceFragmentDelegate mDelegate;
    // UI
    private TextView readDataTextView;
    private EditText writeEditText;

    // GATT
    private BluetoothGattService viscgoGattService;
    private BluetoothGattCharacteristic
            viscgoReadCharacteristic,
            viscgoWriteCharacteristic,
            viscgoNotifyCharacteristic;

    public ViscgoGattServiceFragment() {
        viscgoReadCharacteristic = new BluetoothGattCharacteristic(
                VISCGO_READ_UUID,
                BluetoothGattCharacteristic.PROPERTY_READ,
                BluetoothGattCharacteristic.PERMISSION_READ);

        viscgoWriteCharacteristic = new BluetoothGattCharacteristic(
                VISCGO_WRITE_UUID,
                BluetoothGattCharacteristic.PROPERTY_WRITE | BluetoothGattCharacteristic.PROPERTY_NOTIFY
                        | BluetoothGattCharacteristic.PROPERTY_INDICATE,
                BluetoothGattCharacteristic.PERMISSION_WRITE);
        viscgoWriteCharacteristic.addDescriptor(GattServerActivity.getClientCharacteristicConfigurationDescriptor());

        viscgoNotifyCharacteristic =
                new BluetoothGattCharacteristic(VISCGO_NOTIFY_UUID,
                        BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                        /* No permissions */ 0);
        viscgoNotifyCharacteristic.addDescriptor(
                GattServerActivity.getClientCharacteristicConfigurationDescriptor()
        );

        viscgoGattService = new BluetoothGattService(
                VISCGO_SERVICE_UUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        viscgoGattService.addCharacteristic(viscgoReadCharacteristic);
        viscgoGattService.addCharacteristic(viscgoWriteCharacteristic);
        viscgoGattService.addCharacteristic(viscgoNotifyCharacteristic);
    }

    // Lifecycle callbacks
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_viscgo_gatt, container, false);

        readDataTextView = view.findViewById(R.id.tv_read_data);
        writeEditText = view.findViewById(R.id.et_write);

        writeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String newViscgoLevelString = s.toString();
                // Need to check if the string is empty since isDigitsOnly returns
                // true for empty strings.
                if (!newViscgoLevelString.isEmpty()
                        && android.text.TextUtils.isDigitsOnly(newViscgoLevelString)) {
                    int newViscgoLevel = Integer.parseInt(newViscgoLevelString);
                    if (newViscgoLevel <= VISCGO_LEVEL_MAX) {
                        setViscgoNumber(newViscgoLevelString);
                    } else {
                        Toast.makeText(getActivity(), "Shouldn't exceed 100", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Enter number between 1-100", Toast.LENGTH_SHORT).show();
                }
            }
        });

        writeEditText.setText(String.valueOf(INITIAL_VISCGO_LEVEL));
        setViscgoNumber(String.valueOf(INITIAL_VISCGO_LEVEL));

        final EditText notifyEditText = view.findViewById(R.id.et_notify);

        view.findViewById(R.id.btn_notify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardFrom(getActivity(), writeEditText);
                String notify = notifyEditText.getText().toString();
                if (notify.isEmpty()) {
                    Toast.makeText(getActivity(), "Enter message", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (viscgoNotifyCharacteristic == null) {
                    Toast.makeText(getActivity(), "Viscgo notify characteristic not found", Toast.LENGTH_SHORT).show();
                    return;
                }

                byte[] messageBytes;
                try {
                    messageBytes = notify.getBytes("UTF-8");
                    viscgoNotifyCharacteristic.setValue(messageBytes);
                    mDelegate.sendNotificationToDevices(viscgoNotifyCharacteristic);
                    Log.d("notifyDevices", "success " + notify);
                } catch (UnsupportedEncodingException e) {
                    Log.e("notifyDevices", "Failed to convert " + notify + " string to byte array");
                }
            }
        });

        mDelegate.setConnectionStatus(true);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((CheckBox)view.findViewById(R.id.cb_connection_status))
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()){
                    buttonView.setText(isChecked?"Connected":"Disconnected");
                    mDelegate.setConnectionStatus(isChecked);
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDelegate = (ServiceFragmentDelegate) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ServiceFragmentDelegate");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDelegate = null;
    }

    public BluetoothGattService getBluetoothGattService() {
        return viscgoGattService;
    }

    @Override
    public ParcelUuid getServiceUUID() {
        return new ParcelUuid(VISCGO_SERVICE_UUID);
    }

    private void setViscgoNumber(String newViscgoNumber) {
        byte[] messageBytes;
        try {
            messageBytes = newViscgoNumber.getBytes("UTF-8");
            boolean success = viscgoReadCharacteristic.setValue(messageBytes);
            Log.d("setViscgoNumber", "viscgoReadCharacteristic success " + success + " value:" + newViscgoNumber);
        } catch (UnsupportedEncodingException e) {
            Log.e("setViscgoNumber", "viscgoReadCharacteristic Failed to convert " + newViscgoNumber + " string to byte array");
        }
    }

    @Override
    public void notificationsEnabled(BluetoothGattCharacteristic characteristic, boolean indicate) {
        if (indicate) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), R.string.notificationsEnabled, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    @Override
    public void notificationsDisabled(BluetoothGattCharacteristic characteristic) {

    }

    @Override
    public int writeCharacteristic(final BluetoothGattCharacteristic characteristic, int offset, final byte[] value) {
        if (offset != 0) {
            return BluetoothGatt.GATT_INVALID_OFFSET;
        }
        characteristic.setValue(value);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String viscgoNumber = new String(value, "UTF-8");
                    readDataTextView.setText("Write Data : " + viscgoNumber);
                } catch (UnsupportedEncodingException e) {
                    Log.e("writeCharacteristic", "Unable to convert value bytes to string");
                    e.printStackTrace();
                }
            }
        });
        return BluetoothGatt.GATT_SUCCESS;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
