/*
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cumulations.blePeripheral.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.UUID;

import com.cumulations.blePeripheral.GattServerActivity;
import com.cumulations.blePeripheral.R;


public class BM70GattServiceFragment extends ServiceFragment {

    /* ISSC Proprietary */
    public  static  UUID SERVICE_ISSC_PROPRIETARY  = UUID.fromString("49535343-FE7D-4AE5-8FA9-9FAFD205E455");
    public  static  UUID CHR_CONNECTION_PARAMETER  = UUID.fromString("49535343-6DAA-4D02-ABF6-19569ACA69FE");
    public  static  UUID CHR_ISSC_TRANS_TX         = UUID.fromString("49535343-1E4D-4BD9-BA61-23C647249616");
    public  static  UUID CHR_ISSC_TRANS_RX         = UUID.fromString("49535343-8841-43F4-A8D4-ECBE34729BB3");
    public final static  UUID CHR_ISSC_MP               = UUID.fromString("49535343-ACA3-481C-91EC-D85E28A60318");
    public  static  UUID CHR_ISSC_TRANS_CTRL         = UUID.fromString("49535343-4C8A-39B3-2F49-511CFF073B7E");

    private ServiceFragmentDelegate mDelegate;
    // UI
    private TextView requestReadText,responseSentText;

    // GATT
    private BluetoothGattService bm70ISSCGattService;
    private BluetoothGattCharacteristic
            isscTxCharacteristic,
            isscRxCharacteristic;

    public BM70GattServiceFragment() {
        isscTxCharacteristic = new BluetoothGattCharacteristic(
                CHR_ISSC_TRANS_TX,
                BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                0);
        isscTxCharacteristic.addDescriptor(
                GattServerActivity.getClientCharacteristicConfigurationDescriptor()
        );

        isscRxCharacteristic = new BluetoothGattCharacteristic(
                CHR_ISSC_TRANS_RX,
                BluetoothGattCharacteristic.PROPERTY_WRITE,
                BluetoothGattCharacteristic.PERMISSION_WRITE);

        bm70ISSCGattService = new BluetoothGattService(
                SERVICE_ISSC_PROPRIETARY,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        bm70ISSCGattService.addCharacteristic(isscTxCharacteristic);
        bm70ISSCGattService.addCharacteristic(isscRxCharacteristic);
    }

    // Lifecycle callbacks
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bm70_gatt, container, false);
        requestReadText = view.findViewById(R.id.tv_request_got);
        responseSentText = view.findViewById(R.id.tv_response_sent);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDelegate = (ServiceFragmentDelegate) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ServiceFragmentDelegate");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDelegate = null;
    }

    public BluetoothGattService getBluetoothGattService() {
        return bm70ISSCGattService;
    }

    @Override
    public ParcelUuid getServiceUUID() {
        return new ParcelUuid(SERVICE_ISSC_PROPRIETARY);
    }

    @Override
    public void notificationsEnabled(BluetoothGattCharacteristic characteristic, boolean indicate) {
        if (indicate) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), R.string.notificationsEnabled, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    @Override
    public void notificationsDisabled(BluetoothGattCharacteristic characteristic) {

    }

    @Override
    public int writeCharacteristic(final BluetoothGattCharacteristic characteristic, int offset, final byte[] value) {
        if (offset != 0) {
            return BluetoothGatt.GATT_INVALID_OFFSET;
        }
        characteristic.setValue(value);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                    requestReadText.setText(Arrays.toString(value));
            }
        });
        return BluetoothGatt.GATT_SUCCESS;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public BluetoothGattCharacteristic getIsscTxCharacteristic(){
        return isscTxCharacteristic;
    }

    public void setResponseSentText(byte[] value){
        responseSentText.setText(Arrays.toString(value));
    }
}
